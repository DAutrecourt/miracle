Miracle Calculator

Simple calculator to see the percent chance of drawing a specific card from a Hearthstone deck.
Originally used for the Miracle Rogue deck, can be used with any deck where two copies of the same card are desired.

Currently deprecated, as Miracle Rogue went very out of fashion.