package salt;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;

/**
 * Created by Nick on 8/8/2014.
 * Miracle Calculator for Hearthstone.
 * This currently only works on 1080p screens. This is unlikely to change as this code is not going to be updated any
 * time soon.
 */
public class Main extends JFrame implements ActionListener {
    private JButton exit;
    private JButton reset;
    private JButton gotGadz;
    private JButton gotCard;
    private JLabel cardsLeft;
    private JLabel gadzLeft;
    private JLabel gadzChance;
    private JLabel salterino;
    private BufferedImage BGImage;

    private int numGadz;
    private int numCards;
    private DecimalFormat df;

    public static void main(String[] args){
        new Main();
    }

    Main(){
        numCards = 30;
        numGadz = 2;
        df = new DecimalFormat("#.##");

        try {
            try {
                BGImage = ImageIO.read(new File(getClass().getResource("static/bg.png").toURI()));
            } catch (NullPointerException e) {
                BGImage = ImageIO.read(new File("static/bg.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        buildFrame();
    }

    private String findChance() {
        String chance;
        double p = (double) numGadz * 100 / numCards;
        chance = df.format(p);
        return chance;
    }

    private void buildFrame() {
        setTitle("Miracle");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(0, 300);
        setResizable(false);
        setAlwaysOnTop(true);
        setUndecorated(true);
        setSize(305,395);
        setBackground(new Color(0, 255, 0, 0));

        JLayeredPane layers = new JLayeredPane();
        layers.setSize(305, 395);
        setLayeredPane(layers);

        JLabel BG = new JLabel(new ImageIcon(BGImage));
        BG.setBounds(0, 0, 305, 395);
        layers.add(BG,1);

        cardsLeft = new JLabel("Cards Left: " + numCards);
        cardsLeft.setBounds(165,275, 100,20);
        cardsLeft.setForeground(Color.WHITE);
        layers.add(cardsLeft, 0);

        gadzLeft = new JLabel("Gadz Left: " + numGadz);
        gadzLeft.setBounds(60,275, 100,20);
        gadzLeft.setForeground(Color.WHITE);
        layers.add(gadzLeft, 0);

        gadzChance = new JLabel("% Chance: " + findChance() + "%");
        gadzChance.setBounds(100, 295, 110, 20);
        gadzChance.setForeground(Color.WHITE);
        layers.add(gadzChance, 0);

        salterino = new JLabel("");
        salterino.setBounds(60, 340, 200,20);
        salterino.setForeground(Color.WHITE);
        layers.add(salterino, 0);

        gotGadz = new JButton("Got Gadz");
        gotGadz.addActionListener(this);
        gotGadz.setBounds(60,320, 85,20);
        gotGadz.setMargin(new Insets(1,1,1,1));
        layers.add(gotGadz, 0);

        gotCard = new JButton("No Gadz");
        gotCard.addActionListener(this);
        gotCard.setBounds(155, 320, 85, 20);
        gotCard.setMargin(new Insets(1, 1, 1, 1));
        layers.add(gotCard, 0);

        exit = new JButton("x");
        exit.addActionListener(this);
        exit.setBounds(255, 25, 20, 20);
        exit.setMargin(new Insets(1,1,1,1));
        layers.add(exit,0);

        reset = new JButton("reset");
        reset.addActionListener(this);
        reset.setBounds(120,18,60,20);
        reset.setMargin(new Insets(1,1,1,1));
        layers.add(reset,0);

        this.getRootPane().setDefaultButton(gotCard);

        updateFrame();
        setVisible(true);
    }

    public void updateFrame(){
        cardsLeft.setText("Cards Left: " + numCards);
        gadzLeft.setText("Gadz Left: " + numGadz);
        gadzChance.setText("% Chance: " + findChance() + "%");
        if (numCards < 20 && numCards > 10 && numGadz == 2){
            salterino.setText("Mildly Salty");
        }else if (numCards < 10 && numCards > 5 && numGadz == 2){
            salterino.setText("Caper Level Salt");
        }else if (numCards < 5 && numGadz == 2){
            salterino.setText("Warning: Salt Level Critical");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object action = e.getSource();
        if (action == gotGadz){
            numGadz--;
            numCards--;
            updateFrame();
        }
        if (action == gotCard){
            numCards--;
            updateFrame();
        }
        if (action == exit){
            System.exit(0);
        }
        if (action == reset){
            numCards = 30;
            numGadz = 2;
            updateFrame();
        }
    }
}